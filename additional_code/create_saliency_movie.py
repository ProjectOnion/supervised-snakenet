import cv2
import os

image_folder = '/mnt/or-ext/collections/dean/saliency_images3'
video_name = '/mnt/or-ext/collections/dean/dean_saliency_movie_yellow.avi'

images = [int(img[5:-4]) for img in os.listdir(image_folder) if img.endswith(".png")]
images = sorted(images, key=int)
images = ["board" + str(image) + ".png" for image in images]
fourcc = cv2.cv.CV_FOURCC(*'XVID')
frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape

video = cv2.VideoWriter(video_name, fourcc, 10.0, (width,height))

for image in images:
    print (image + " "),
    video.write(cv2.imread(os.path.join(image_folder, image)))

video.release()
cv2.destroyAllWindows()

print(video_name + " saved.")
