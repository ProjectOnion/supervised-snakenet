from settings import mini_batch_size, keep_prob_start, amountOfMiniBatchFilesToTest
from utilities import batchGenerator
import tensorflow as tf
import time
import numpy as np
import sys

class SessionTrainer:
	def __init__ (self):
		self.config = tf.ConfigProto(allow_soft_placement=True)
		self.sess = None
		self.graph = None

	def train_minibatches(self, batchData):
		gpuTotalTime = 0
		for miniBatch in batchGenerator(batchData, mini_batch_size):
			gpuStartTime = time.time()
			self.graph.train_step.run(
				feed_dict={self.graph.x: miniBatch[0],
					self.graph.y_: miniBatch[1],
					self.graph.keep_prob: keep_prob_start})
			gpuTotalTime = gpuTotalTime + (time.time() - gpuStartTime)
		return gpuTotalTime

	def validate_minibatches(self, validationBatchData, global_step):
		sumOfValidations = 0
		amountOfValidations = 0
		final_confusion = np.zeros([3,3])
		for miniBatch in batchGenerator(validationBatchData, mini_batch_size):
			validate_accuracy = self.graph.accuracy.eval(feed_dict={
				self.graph.x: miniBatch[0],
				self.graph.y_: miniBatch[1],
				self.graph.keep_prob: 1.0})
			final_confusion = final_confusion + self.graph.confusion.eval(feed_dict={
				self.graph.x: miniBatch[0],
				self.graph.y_: miniBatch[1],
				self.graph.keep_prob: 1.0})
			sumOfValidations = sumOfValidations + validate_accuracy
			amountOfValidations = amountOfValidations + 1

		print("global step " + str(global_step.eval()) +\
			" validation on SLR data: "  + str(sumOfValidations/amountOfValidations))
		print("global step " + str(global_step.eval()) +\
			 " confusion matrix on SLR data:\n" + str(final_confusion))
		sys.stdout.flush()

	def test_minibatches(self, data):
		sumOfTests = 0
		amountOfTests = 0
		final_confusion = np.zeros([3,3])
		for i in range(1, amountOfMiniBatchFilesToTest + 1):
			data.prepare_test_data(i)
			for miniBatch in batchGenerator(data.testData, mini_batch_size):
				test_accuracy = self.graph.accuracy.eval(feed_dict={
					self.graph.x: miniBatch[0],
					self.graph.y_: miniBatch[1],
					self.graph.keep_prob: 1.0})
				final_confusion = final_confusion + self.graph.confusion.eval(feed_dict={
					self.graph.x: miniBatch[0],
					self.graph.y_: miniBatch[1],
					self.graph.keep_prob: 1.0})
				sumOfTests = sumOfTests + test_accuracy
				amountOfTests = amountOfTests + 1

		print("test accuracy: " + str(sumOfTests / amountOfTests))
		print("global step " + str(global_step.eval()) +\
			 " confusion matrix on SLR data:\n" + str(final_confusion))
		sys.stdout.flush()


